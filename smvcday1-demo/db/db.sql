CREATE SCHEMA `demo` DEFAULT CHARACTER SET utf8mb4;

CREATE TABLE users (
	user_id INTEGER AUTO_INCREMENT,
	id VARCHAR(32) NOT NULL,
	password VARCHAR(64) NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY(user_id)
);

-- Never add plain text passwords in production!
INSERT INTO users (id, password)
VALUES ('admin', 'admin');