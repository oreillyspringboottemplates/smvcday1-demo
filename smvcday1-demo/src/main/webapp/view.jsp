<!doctype html>
<html lang="en">
<head>
	<title>Spring MVC Demo</title>
</head>
<body>
	<form action="/login" method="POST">
		<input id="id" name="id" />
		<input id="password" name="password" type="password" />
		<button>Submit</button>
		<p>
			<%
				try {
					String errorMessage = (String)session.getAttribute("error");
					if (errorMessage != null && errorMessage.length() > 0) {
						out.println(errorMessage);		
					}	
				} finally {
					session.removeAttribute("error");
				}
			%>
		</p>
	</form>
</body>
</html>