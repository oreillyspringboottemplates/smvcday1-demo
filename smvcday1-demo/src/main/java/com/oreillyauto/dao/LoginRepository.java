package com.oreillyauto.dao;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class LoginRepository extends JdbcDaoSupport {
	@Autowired 
	private DataSource dataSource;
    
	@PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
    public boolean isAuthenticated(String id, String password) {
    	// Build your SQL
        String sql = "SELECT count(*) " +
                     "  FROM users " +
                     " WHERE id = '" + id + "' " +
                     "   AND password = '" + password + "'";
        
        // Get the row count that matches the SQL/predicate above. 
        int count = getJdbcTemplate().queryForObject(sql, Integer.class);
        
        // If we find that the count is greater than 0 then
        // the id and password must be correct. User is authenticated.
        return count > 0;
    }
}
