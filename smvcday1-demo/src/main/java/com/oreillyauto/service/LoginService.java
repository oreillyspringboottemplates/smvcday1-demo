package com.oreillyauto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.LoginRepository;

@Service
public class LoginService {
	
    @Autowired
    LoginRepository loginRepo;

    public boolean postLogin(String id, String password) {
    	
    	// Normally in our service layer, we would convert the plain string
    	// to a one-way hash (from the plain text password submitted via HTTPS 
    	// in the form). Normally developers use BCrypt.
    	// For this example, though, we will simply send the plain password
    	// without creating the hash.
        boolean isAuthenticated = loginRepo.isAuthenticated(id, password); 
        return isAuthenticated;
    }
}
