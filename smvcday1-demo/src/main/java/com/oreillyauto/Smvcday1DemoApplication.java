package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Smvcday1DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Smvcday1DemoApplication.class, args);
	}

}
