package com.oreillyauto.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.oreillyauto.service.LoginService;

@Controller
public class LoginController {
	
    @Autowired
    LoginService loginService;
    
    @GetMapping(value="/login")
    public String getLoginPage(HttpSession session) {
    	if ("true".equalsIgnoreCase((String)session.getAttribute("loggedIn"))) {
    		return "redirect:/dashboard";
    	}
    	
    	return "view.jsp";
    }
        
    @PostMapping(value="/login")
    public String postLogin(String id, String password, HttpSession session) {
        if (loginService.postLogin(id, password)) {
        	session.setAttribute("loggedIn", "true");
            return "redirect:/dashboard";
        } else {
        	session.setAttribute("error", "Invalid credentials. Please try again.");
            return "redirect:/login";
        }
    }
    
    @GetMapping(value="/dashboard")
    public String getDashboardPage(HttpSession session) {
    	if ("true".equalsIgnoreCase((String)session.getAttribute("loggedIn"))) {
    		return "dashboard.html";	
    	} else {
    		return "redirect:/login";
    	}
    }
    
    @GetMapping(value="/logout")
    public String getLogout(HttpSession session) {
    	session.removeAttribute("loggedIn");
        return "redirect:/login";
    }
}
